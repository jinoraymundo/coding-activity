# Coding Activity - Jino Raymundo
As per instructions from here: https://docs.google.com/document/d/1DiYkafWR4O0Pl3kjgyZp1RArUuONDQp73ko14QZ16tU/edit

### Software and Versions

| Software | Version |
| ------ | ------ |
| Python | 3.6.1 |
| Django | 1.11.6 |
| Django Rest Framework | 3.7.1 |
| django-oauth-toolkit | 1.0.0 |

### Installation

```sh
$ git clone git@bitbucket.org:jinoraymundo/coding-activity.git
$ cd coding-activity
$ pyenv local 3.6.1
$ python -m venv venv
$ source venv/bin/activate
$ ./manage.py migrate
$ ./manage.py createsuperuser
// login by accessing http://localhost:8000/admin via web browser
// register an application via http://localhost:8000/o/applications/
Name: <any name>
Client Type: confidential
Authorization Grant Type: Resource owner password-based
Save it then supply CLIENT_ID and CLIENT_SECRET to .env
$ ./manage.py test
$ ./manage.py runserver
(optional)
$ vim .env
```

### .env
```
CLIENT_ID=
CLIENT_SECRET=
EMAIL_HOST=
EMAIL_PORT=
EMAIL_HOST_USER=
EMAIL_HOST_PASSWORD=
```
