from rest_framework import permissions
from rest_framework.permissions import BasePermission

from django.contrib.auth import get_user_model


class IsAuthenticatedOrCreate(permissions.IsAuthenticated):
    def has_permission(self, request, view):
        if request.method == 'POST':
            return True

        if view.action in ['list', 'is_active']:
            return True

        return super(IsAuthenticatedOrCreate, self).has_permission(
            request, view)


class IsOwner(BasePermission):

    def has_object_permission(self, request, view, obj):
        if view.action == 'partial_update':
            return obj.id == request.user.id
        return True
