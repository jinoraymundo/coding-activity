from django.test import TestCase
from django.core.urlresolvers import reverse
from django.contrib.auth import get_user_model
from django.contrib.auth.hashers import make_password

from rest_framework import status
from rest_framework.authtoken.models import Token
from rest_framework.renderers import JSONRenderer
from rest_framework.test import APIClient, force_authenticate

from api.serializers import UserSerializer


class ModelTestCase(TestCase):

    def setUp(self):
        self.user = get_user_model()(email="jinoraymundo@gmail.com")

    def test_model_can_create_a_user(self):
        old_count = get_user_model().objects.count()
        self.user.save()
        new_count = get_user_model().objects.count()
        self.assertNotEqual(old_count, new_count)


class ViewTestCase(TestCase):

    def setUp(self):
        self.client = APIClient()
        self.response = self.client.post(
            reverse('user-list'),
            {
                'email': 'jinoraymundo@gmail.com',
                'password': 'jin',
            },
            format="json")


    def test_api_can_create_a_user(self):
        self.assertEqual(self.response.status_code, status.HTTP_201_CREATED)


    def test_api_can_get_a_user(self):
        user = get_user_model().objects.get()
        self.client.force_authenticate(user=user)
        response = self.client.get(
            reverse('user-detail', kwargs={'pk': user.id}),
            format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, user)


    def test_api_can_update_a_user(self):
        user = get_user_model().objects.get()
        self.client.force_authenticate(user=user)
        change_user = {
            'email': 'yoda@jedi.org',
            'password': 'lightsaber',
        }
        res = self.client.put(
            reverse('user-detail', kwargs={'pk': user.id}),
            change_user,
            format="json")

        self.assertEqual(res.status_code, status.HTTP_200_OK)


    def test_api_can_delete_user(self):
        user = get_user_model().objects.get()
        self.client.force_authenticate(user=user)
        response = self.client.delete(
            reverse('user-detail', kwargs={'pk': user.id}),
            format="json",
            follow=True)

        self.assertEquals(response.status_code, status.HTTP_204_NO_CONTENT)


    def test_api_registration_user_not_active(self):
        user = get_user_model().objects.get()
        self.assertFalse(user.is_active)


    def test_api_registration_check_token(self):
        user = get_user_model().objects.get()
        token = Token.objects.get(user=user)
        self.assertEqual(user.auth_token, token)


    def test_api_user_activation(self):
        user = get_user_model().objects.get()
        response = self.client.get(
            reverse('user-is-active', kwargs={
                'pk': user.id,
            }),
            {'token': str(user.auth_token)},
            format="json",
            follow=True)
        user.refresh_from_db()
        self.assertTrue(user.is_active)


    def test_api_change_password(self):
        user = get_user_model().objects.get()
        response = self.client.patch(
            reverse('user-detail', kwargs={'pk': user.id}),
            format="json",
            follow=True)

        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        self.client.force_authenticate(user=user)
        data = {
            'current_password': make_password('jin'),
            'new_password': make_password('NEW_PASSWORD'),
        }
        response = self.client.patch(
            reverse('user-detail', kwargs={'pk': user.id}),
            data,
            format="json",
            follow=True)

        user.refresh_from_db()
        self.assertTrue(response.status_code, status.HTTP_200_OK)
        self.assertNotEqual(user.password, 'jin')
