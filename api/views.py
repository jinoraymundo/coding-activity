import json
import requests

from urllib.parse import urlencode

from django.conf import settings
from django.contrib.auth import get_user_model
from django.core.mail import send_mail

from oauth2_provider.views.mixins import OAuthLibMixin

from requests.auth import HTTPBasicAuth

from rest_framework import status, generics
from rest_framework.decorators import list_route, detail_route
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from api.permissions import IsAuthenticatedOrCreate, IsOwner
from api.serializers import UserSerializer, LimitedUserSerializer


class UserViewSet(ModelViewSet, OAuthLibMixin):
    serializer_class = UserSerializer
    queryset = get_user_model().objects.all()
    permission_classes = [IsAuthenticatedOrCreate, IsOwner, ]

    def get_serializer_class(self):
        if self.request.user.is_authenticated:
            return UserSerializer
        return LimitedUserSerializer


    @detail_route(methods=['get'])
    def is_active(self, request, pk=None, *args, **kwargs):
        user = self.get_object()
        submitted_token = request.GET.get('token')

        if submitted_token == str(user.auth_token):
            user.is_active = True
            user.save()
            serializer = UserSerializer(user)
            return Response(serializer.data,
                            status=status.HTTP_200_OK)
        return Response({
            'message': 'Invalid Token',
        }, status=status.HTTP_400_BAD_REQUEST)



    @list_route(methods=['post'])
    def login(self, request):
        username = request.data.get('username')
        password = request.data.get('password')

        user = get_user_model().objects.get(email=username)

        if not user.is_active:
            return Response({
                'message': 'User is not activated yet, kindly check your email.'
            }, status=status.HTTP_400_BAD_REQUEST)

        if user.check_password(password):
            token_url = '{}://{}:{}/o/token/'.format(
                request.scheme,
                request.META['SERVER_NAME'],
                request.META['SERVER_PORT'])
            r = requests.post(
                    token_url,
                    data={
                        'grant_type': 'password',
                        'username': user.email,
                        'password': password,
                    },
                    auth=HTTPBasicAuth(settings.CLIENT_ID,
                        settings.CLIENT_SECRET)
                )
            try:
                return Response({
                    'access_token': r.json()['access_token']
                }, status=status.HTTP_200_OK)
            except KeyError:
                return Response({
                    'message': 'OAuth2 Token creation error'
                }, status.HTTP_400_BAD_REQUEST)
        return Response({
            'message': 'Incorrect Credentials'
        }, status.HTTP_400_BAD_REQUEST)


    def partial_update(self, request, pk=None):
        user = self.get_object()
        if not user:
            return Response(status=status.HTTP_404_NOT_FOUND)

        current_password = request.data.get('current_password')
        new_password = request.data.get('new_password')

        if user.check_password(current_password):
            user.set_password(new_password)
            user.save()

            serializer = self.get_serializer(
                user,
                data=request.data,
                partial=True)

            if serializer.is_valid():
                return Response(serializer.data,
                                status=status.HTTP_200_OK)
        return Response({
            'message': 'Password is incorrect'
        }, status=status.HTTP_400_BAD_REQUEST)


    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.save()
        send_mail(
            'Coding Activity: Activate your account',
            '{}://{}:{}/users/{}/is_active/?{}'.format(
                request.scheme,
                request.META['SERVER_NAME'],
                request.META['SERVER_PORT'],
                user.id,
                urlencode({'token': str(user.auth_token)}),
            ),
            'email@coding-activity.com',
            [user.email, ],
        )
        return Response(serializer.data, status=status.HTTP_201_CREATED)
