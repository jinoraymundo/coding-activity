from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
from rest_framework.routers import SimpleRouter

from api.views import UserViewSet


router = SimpleRouter()
router.register("users", UserViewSet)

urlpatterns = router.urls
urlpatterns = format_suffix_patterns(urlpatterns)
