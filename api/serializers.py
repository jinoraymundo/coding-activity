from rest_framework import serializers
from rest_framework.authtoken.models import Token

from django.contrib.auth import get_user_model


class UserSerializer(serializers.ModelSerializer):

    email = serializers.EmailField(max_length=100)
    first_name = serializers.CharField(allow_blank=True, required=False)
    last_name = serializers.CharField(allow_blank=True, required=False)
    password = serializers.CharField(
        style={'input_type': 'password'},
        write_only=True
    )
    
    class Meta:
        model = get_user_model()
        fields = ('id', 'email', 'password', 'first_name', 'last_name',
                  'is_active', )


    def create(self, validated_data):
        user = get_user_model()(
            email=validated_data['email'],
            first_name=validated_data.get('first_name', ''),
            last_name=validated_data.get('last_name', ''),
        )
        user.set_password(validated_data['password'])
        user.save()
        return user


class LimitedUserSerializer(serializers.ModelSerializer):

    email = serializers.EmailField(max_length=100)

    class Meta:
        model = get_user_model()
        fields = ('id', 'email', 'first_name', 'last_name',
                  'password', )
        extra_kwargs = {
            'last_name': {'write_only': True, },
            'password': {'write_only': True, },
            'email': {'write_only': True, },
        }
        read_only_fields = ('is_active', )

    def create(self, validated_data):
        user = get_user_model()(
            email=validated_data.get('email', ''),
            first_name=validated_data.get('first_name', ''),
            last_name=validated_data.get('last_name', ''),
        )
        user.set_password(validated_data['password'])
        user.save()
        return user
